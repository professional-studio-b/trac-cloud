"""Imports"""
import json
import os
from decimal import Decimal
from datetime import datetime, timedelta
from collections import defaultdict

import boto3
from boto3.dynamodb.conditions import Key

DYNAMODB = boto3.resource('dynamodb')
DATE_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'


def get_samples_table():
    """Returns the Samples DynamoDB Table"""
    # Environment variables are passed in by the cloudformation stack
    key = os.environ["SamplesTable"]
    return DYNAMODB.Table(key)


def get_ranks_table():
    """Returns the Ranks DynamoDB Table"""
    # Environment variables are passed in by the cloudformation stack
    key = os.environ["RanksTable"]
    return DYNAMODB.Table(key)


def collect(event, context):
    """Function provides functionality to accept data from sensor"""
    table = get_samples_table()

    # Event contains the parsed JSON body
    body = event['body']
    # floats are not supported in dynamodb, need to convert to decimal
    body = json.loads(body, parse_float=Decimal)

    with table.batch_writer() as batch:
        for sample in body['samples']:
            batch.put_item(
                Item={
                    "lineId": body["lineId"],
                    "datetime": sample["dateTime"],
                    "xAccel": sample["xAccel"],
                    "yAccel": sample["yAccel"],
                    "zAccel": sample["zAccel"],
                    "gpsLat": Decimal(sample["gpsLat"]),
                    "gpsLon": Decimal(sample["gpsLon"])
                }
            )

    return {
        'statusCode': 200,
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        'body': json.dumps({'result': 'OK'})
    }


def load_most_recent_ranks():
    """
    Load a week worth of rankings and find the most recent for each id - we can't
    select the most recent values directly, but since ranks are calculated daily
    this will cover us (and allows us to have a line unsampled for a few days
    without error)
    """

    ranks = get_ranks_table()

    date_min = datetime.now() - timedelta(days=7)
    expr = Key('date').between(stringify_date(date_min),
                               stringify_date(datetime.now()))
    result = ranks.scan(FilterExpression=expr)

    recent_ranks = defaultdict(lambda: {'date': date_min, 'rank': 0})
    for row in result["Items"]:
        line_id, date = row['lineId'], parse_date(row['date'])

        top = recent_ranks[line_id]
        if date > top['date']:
            top['date'] = date
            top['rank'] = row['rank']

    return recent_ranks


def lines(event, context):
    """Returns all lines and their most recent ranks"""
    ranks = load_most_recent_ranks()
    result = [{
        'lineId': line_id,
        'rank': value
    } for line_id, value in ranks.items()]

    for line in result:
        line['rank']['date'] = stringify_date(line['rank']['date'])

    return {
        'statusCode': 200,
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        'body': json.dumps({'lines': result})
    }


def parse_date(string_date):
    """Parse a date formatted by the JS standard"""
    return datetime.strptime(string_date, DATE_FORMAT)


def stringify_date(date):
    """Return the date formatted by the JS standard"""
    return date.strftime(DATE_FORMAT)


def get_or_default(dic, key, transform, default):
    """
    Gets the value from the dictionary and applies f to it.
    If it doesnt exist return 'default'
    """
    if key in dic and len(dic[key]) > 0:
        return transform(dic[key])
    return default


def lineranks(event, context):
    """Get the ranks for a line over a given date range"""
    line_id = event['pathParameters']['id']
    params = event['queryStringParameters']
    time_from = get_or_default(params, 'from', parse_date,
                               datetime.now() - timedelta(days=30))
    time_to = get_or_default(params, 'to', parse_date, datetime.now())

    if time_from >= time_to:
        error_msg = "The 'from' date must be equal to or before the 'to' date"
        return {
            'statusCode': 400,
            'body': json.dumps({'message': error_msg})
        }

    ranks = get_ranks_table()
    date_expr = Key('date').between(stringify_date(time_from), stringify_date(time_to))
    id_expr = Key('lineId').eq(line_id)

    result = ranks.query(KeyConditionExpression=id_expr & date_expr)

    return {
        'statusCode': 200,
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        'body': json.dumps({'rankings': result["Items"]})
    }


def translate_sample(sample):
    """Translate a sample from the DB into the result dictated by our swagger"""
    return {
        "datetime": sample["datetime"],
        "xAccel": float(sample["xAccel"]),
        "yAccel": float(sample["yAccel"]),
        "zAccel": float(sample["zAccel"]),
        "gpsLat": str(sample["gpsLat"]),
        "gpsLon": str(sample["gpsLon"])
    }


def linesamples(event, context):
    """Gets the raw line samples for the specified date range"""
    line_id = event['pathParameters']['id']
    params = event['queryStringParameters']
    time_from = get_or_default(params, 'from', parse_date,
                               datetime.now() - timedelta(days=1))
    time_to = get_or_default(params, 'to', parse_date,
                             datetime.now() + timedelta(days=1))

    date_expr = Key('datetime').between(stringify_date(time_from), stringify_date(time_to))
    id_expr = Key('lineId').eq(line_id)

    table = get_samples_table()
    response = table.query(KeyConditionExpression=id_expr & date_expr)

    return {
        'statusCode': 200,
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        'body': json.dumps({
            "samples": [translate_sample(sample) for sample in response["Items"]]
        })
    }
